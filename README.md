# What is this

This projec is a short Android development experiment. It is
a simple dating application that can be used to find company
in a bar or other limited area. The communication is based on
UDB broadcasts (JSON format) within area of a single Wi-Fi
segment.

At least two Android devices will be required to test this 
application. All phones must be connected to the same Wi-Fi.

All devices will send beacon messages repeatedly and at the
same time they will listen to UDB broadcasts on thensame port.
When a message is received it will be compared against the local
settings. If the match the peer will be shown on a list and
a chat can be started.

There is no server. Instead everything is based on
point to point connections between peers. If Wi-Fi connectivity
is lost then it will be no longer possibility to continue
conversation (this is intentional feature).

## How to use

There are three screens in this app. The first one is used to set
user information (nick, sex, age) and search criteria (min/max age
and desired sex). After that the start button can be used to activate
the communication and to move to the second screen.

If matching contacts are found their nick and age will be shown on the screen.
By clicking the name it is possible to start a chat.

## Status

This project was just to study Android development. It is in no way
in production level. It is bit ugly but it works. The biggest problem is that 
communication stops when the phone goes to sleep.

## License

This is unpublished work. All rights reserved.
