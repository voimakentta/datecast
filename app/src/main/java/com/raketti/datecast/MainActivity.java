package com.raketti.datecast;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.DhcpInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.raketti.datecast.ui.main.BroadcastReceiver;
import com.raketti.datecast.ui.main.BroadcastSender;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Random;

public class MainActivity extends AppCompatActivity implements SecondFragment.OnFragmentInteractionListener {
    GlobalData g = GlobalData.getInstance();
    JSONObject jsonObject = g.getJson();
    static String broadcastIp;

    public static MainActivity mainActivity;

    public MainActivity() {
        mainActivity = this;
    }

    NotificationManager notificationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        loadSettings();

        notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        createNotificationChannel("com.raketti.datecast.contact", "New datecast contact", "Datecast Contact Channel");

        WifiManager wifiManager = (WifiManager)getSystemService(WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        DhcpInfo dhcpInfo = wifiManager.getDhcpInfo();

        Log.i(g.TAG, "Wi-Fi SSID=" + wifiInfo.getSSID());
        int ip;
        ip = wifiInfo.getIpAddress();

        if (ip == 0) {
            Log.e(g.TAG, "No WIFI IP address");
            g.setWifiOk(false);
            return;
        }

        g.setWifiOk(true);
        Log.i(g.TAG, "Got IP=" + Integer.toHexString(ip));

        ip = (ip & 0xffffff) | 0xff000000; // Broadcast IP
        g.setIp(ip);

        Intent intent = new Intent(this, BroadcastSender.class);
        startForegroundService(intent);

        intent = new Intent(this, BroadcastReceiver.class);
        startForegroundService(intent);
    }

    protected void createNotificationChannel(String id, String name, String description) {
        int importance = NotificationManager.IMPORTANCE_LOW;
        NotificationChannel channel = new NotificationChannel(id, name, importance);

        channel.setDescription(description);
        channel.enableLights(true);
        channel.setLightColor(Color.RED);
        channel.enableVibration(true);
        channel.setVibrationPattern(new long[]{100, 200, 300, 400, 500,400, 300, 200,400});
        notificationManager.createNotificationChannel(channel);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    public void sendNotification(int uid, String title, String text) {
        String channelID = "com.raketti.datecast.contact";

        Notification.Builder builder = new Notification.Builder(MainActivity.this, channelID);
        builder.setContentTitle(title);
        builder.setContentText(text);
        builder.setSmallIcon(android.R.drawable.ic_dialog_info);
        builder.setChannelId(channelID);
        builder.setNumber(10)
        .setColor(Color.RED);
        Notification notification =
                builder
                .build();

        notificationManager.notify(uid, notification);
    }

    private boolean loadSettings() {
        SharedPreferences sharedPreferences =
                PreferenceManager.getDefaultSharedPreferences(getApplicationContext());


        int uid = sharedPreferences.getInt("uid", -1);

        if (uid == -1) {
            Log.i(g.getTAG(), "loadSettings: No settings yet");

            final int random = new Random().nextInt(2000000000);
            Log.i(g.getTAG(), "New UID " + Integer.toString(random));

            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putInt("uid", random);
            editor.commit();
            uid = random;
            g.setUid(uid);
            try {
                jsonObject.put("uid", uid);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return false;
        } else {
            Log.i(g.getTAG(), "Loaded UID " + Integer.toString(uid));
            try {
                jsonObject.put("uid", uid);
                g.setUid(uid);

                String nick = sharedPreferences.getString("nick", "My name");
                int age = sharedPreferences.getInt("age", 47);
                int mysex = sharedPreferences.getInt("mysex", 0);
                int minAge = sharedPreferences.getInt("minage", 99);
                int maxAge = sharedPreferences.getInt("maxage", 120);
                int interest = sharedPreferences.getInt("interest", g.SEX_FEMALE | g.SEX_MALE);

                jsonObject.put("nick", nick);
                Log.i(g.getTAG(), "Put nick "+ nick);
                jsonObject.put("age", age);
                jsonObject.put("mysex", mysex);
                jsonObject.put("minage", minAge);
                jsonObject.put("maxage", maxAge);
                jsonObject.put("interest", interest);

            } catch (JSONException e) {
                Log.i(g.getTAG(), "Json error");
                e.printStackTrace();
            }
        }

        return true;
    }

}
