package com.raketti.datecast;

import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.view.KeyEvent;

import com.raketti.datecast.UserData;
import com.raketti.datecast.GlobalData;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.text.Format;

import static com.raketti.datecast.MainActivity.mainActivity;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link chatFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class chatFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private GlobalData g = GlobalData.getInstance();

    private UserData userData;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public chatFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment chatFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static chatFragment newInstance(String param1, String param2) {
        chatFragment fragment = new chatFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        g.updateChat();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_chat, container, false);

        TextView tv = (TextView)v.findViewById(R.id.peerName);

        userData = g.getPeerIndex(g.getCurrentPeer());

        tv.setTextSize(30F);
        g.updatePeer(tv, userData);

        final EditText chatField = (EditText)v.findViewById(R.id.chatText);
        final TextView tw = (TextView)v.findViewById(R.id.chatView);

        chatField.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_DOWN &&
                   keyCode == KeyEvent.KEYCODE_ENTER) {
                    Log.i(g.TAG, "Enter pressed");
                    String line = chatField.getText().toString();

                    String msg;

                    String chatText = userData.chat;

                    if (chatText != "")
                        chatText += "\n";
                    chatText += "<- " + line;
                    tw.setText(chatText);
                    userData.chat = chatText;

                    try {
                        sendChat(line, userData.ip);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    chatField.setText("");
                    return true;
                }
                return false;
            }
        });

        return v;
    }

    private void sendChat(String chatMsg, final InetAddress ip) throws JSONException {
        final JSONObject json = g.getJson();

        json.put("event", 1);
        json.put("msg", chatMsg);
        final String messageStr = g.getJson().toString();

        new Thread( new Runnable() {
            public void run() {
                try {
                    DatagramSocket s = new DatagramSocket();
                    int msg_length;
                    msg_length = messageStr.length();
                    byte[] message = messageStr.getBytes();

                    DatagramPacket p = new DatagramPacket(message, msg_length, ip, g.getPort());
                    s.send(p);
                    Log.d("datecast", "message sent to " + ip.toString());
                } catch (Exception e) {
                    Log.d("datecast", "error  " + e.toString());
                }
            }
        }).start();

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
}
