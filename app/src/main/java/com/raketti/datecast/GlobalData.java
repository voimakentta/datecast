package com.raketti.datecast;

import android.annotation.SuppressLint;
import android.app.Application;
import android.app.Notification;
import android.graphics.Color;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.nfc.FormatException;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.navigation.Navigation;

import org.json.JSONObject;

import java.text.Format;
import java.util.Calendar;
import java.util.Formatter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import static com.raketti.datecast.MainActivity.mainActivity;

public class GlobalData extends Application {
    private static GlobalData instance = new GlobalData();
    public String TAG = "dateCast";
    private int port = 50008; //port that I’m using
    private int ip = 0;
    private JSONObject json = new JSONObject();
    private int age;
    private int sex;
    private int minAge;
    private int maxAge;
    private int sexMask;
    private int uid = -1;
    private String hello = null;
    private boolean wifiOk = false;
    private Notification notification = null;

    public Notification getNotification() {
        return notification;
    }

    public void setNotification(Notification notification) {
        this.notification = notification;
    }

    public boolean isWifiOk() {
        return wifiOk;
    }

    public void setWifiOk(boolean wifiOk) {
        this.wifiOk = wifiOk;
    }

    private Map<Integer, UserData> peers = new HashMap<>();

    private UserData[] peerIndex = new UserData[5];// TODO: do not use hardcoded limit
    private int currentPeer = -1;
    private int numPeers = 0;

    public static final int SEX_MALE = 1;
    public static final int SEX_FEMALE = 2;

    public int getNumPeers() {
        return numPeers;
    }

    public void setNumPeers(int numPeers) {
        this.numPeers = numPeers;
    }

    public static GlobalData getInstance() {
        return instance;
    }

    public JSONObject getJson() {
        return json;
    }

    public void setSexMask(int mask) {
        sexMask = mask;
    }

    public int getSexMask() {
        return sexMask;
    }

    public void setMinAge(int age) {
        minAge = age;
    }

    public int getMinAge() {
        return minAge;
    }

    public int getUid() {
        return uid;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public void setMaxAge(int age) {
        maxAge = age;
    }

    public int getMaxAge() {
        return maxAge;
    }

    public void setHello(String hello) {
        Log.i(getTAG(), "Hello=" + hello);
        this.hello = hello;
    }

    public String getHello() {
        return hello;
    }

    public String getTAG() {
        return TAG;
    }

    public void setTAG(String TAG) {
        this.TAG = TAG;
    }

    public int getPort() {
        return port;
    }

    public int getIp() {
        return ip;
    }

    public void setIp(int ip) {
        this.ip = ip;
    }

    public void putPeer(int uid, UserData p) {
        peers.put(uid, p);
    }

    public UserData getPeer(int uid) {

        if (!peers.containsKey(uid))
            return null;

        return peers.get(uid);
    }

    public UserData getPeerIndex(int n) {
        return peerIndex[n];
    }

    public int getCurrentPeer() {
        return currentPeer;
    }

    public void setCurrentPeer(int currentPeer) {
        this.currentPeer = currentPeer;
    }

    public Map<Integer, UserData> getPeers() {
        return peers;
    }

    public void updateScreen() {
        mainActivity.runOnUiThread(new Runnable() {
            @SuppressLint("SetTextI18n")
            public void run() {
                Log.i(TAG, "Update screen");
                TextView tv;

                int i;

                i = 0;

                Iterator<Map.Entry<Integer,UserData>> iterator = peers.entrySet().iterator();
                while (iterator.hasNext()) {
                    tv = mainActivity.findViewById(R.id.textItem0 + i);

                    if (tv == null) {
                        // Not in the right screen just now
                        updateChat();
                        return;
                    }

                    Map.Entry<Integer,UserData> peer = iterator.next();
                    peerIndex[i] = peer.getValue();

                    long delay = (Calendar.getInstance().getTimeInMillis() - peer.getValue().time) / 60000;

                    if (delay > 5)  // Has disappeared
                        continue;

                    tv.setTextSize(30F);
                    tv.setMovementMethod(new ScrollingMovementMethod());

                    updatePeer(tv, peer.getValue());

                    tv.setOnClickListener(
                            new Button.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    int n = v.getId()-R.id.textItem0;
                                    Log.i(TAG, "Click click " + n);
                                    if (n >= numPeers)
                                        return;

                                    setCurrentPeer(n);
                                    // Go to chat view
                                    Navigation.findNavController(v).navigate(R.id.secondToChat);
                                }
                            }
                    );
                    i++;
                }

                setNumPeers(i);

                while (i < 5) {
                    tv = mainActivity.findViewById(R.id.textItem0 + i);
                    tv.setText("");
                    i++;
                }
            }
        });
    }

    public void updatePeer(TextView tv, UserData peer)
    {

        if (peer.sex == SEX_MALE) {
            tv.setTextColor(Color.BLUE);
        } else {
            tv.setTextColor(Color.RED);
        }

        long delay = (Calendar.getInstance().getTimeInMillis() - peer.time) / 60000;
        String when = "active";

        if (delay > 0) {
            if (delay == 1) {
                when = "inactive";
            } else {
                when = "lost connection";
                tv.setTextColor(Color.GRAY);
            }
        }
        tv.setText(peer.nick+ ", " + peer.age + " " + when);
    }

   public void updateChat() {
        TextView tv = mainActivity.findViewById(R.id.chatView);
        if (tv == null)
            return;

        int peer = getInstance().getCurrentPeer();
        if (peer < 0 || peer >= numPeers)
            return;

        UserData u = getInstance().getPeerIndex(peer);
        Log.i(getInstance().TAG, "updateChat() " + u.nick);
        tv.setText(u.chat);

        tv = (TextView)mainActivity.findViewById(R.id.peerName);
        updatePeer(tv, u);
    }
}
