package com.raketti.datecast.ui.main;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.net.DhcpInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.text.format.Formatter;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.raketti.datecast.GlobalData;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.text.Format;

import static android.content.Context.WIFI_SERVICE;
import static com.raketti.datecast.MainActivity.mainActivity;

class SenderTask extends AsyncTask<Integer, Integer, String> {
    GlobalData g = GlobalData.getInstance();

    @Override
    protected String doInBackground(Integer... params) {
        int startId = params[0];

        while (true) {
            int ip = 0;
            WifiManager wifiManager;

            String hello = g.getHello();

            try {
                wifiManager = (WifiManager) mainActivity.getSystemService(WIFI_SERVICE);
                WifiInfo wifiInfo = wifiManager.getConnectionInfo();
                ip = wifiInfo.getIpAddress();
            } catch (Exception e) {
                wifiManager = null;
                Log.e(g.TAG, "Wifi manager access failed");
            }

            if (ip == 0) {
                g.setWifiOk(false);
                Log.e(g.TAG, "No WIFI IP address");
                try {
                    Thread.sleep(60*1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                continue;
            }

            g.setWifiOk(true);
            DhcpInfo dhcpInfo = wifiManager.getDhcpInfo();
            if (dhcpInfo != null) {
                int netmask = dhcpInfo.netmask;
                Log.i(g.TAG, "Netmask=" + Integer.toHexString(netmask));
                ip = (ip & netmask) | ~netmask;
            } else {
                ip = (ip & 0xffffff) | 0xff000000; // 8 bit broadcast IP
                Log.i(g.TAG, "Use default netmask");
            }
            g.setIp(ip);

            // TODO: Alert user if no Wi-Fi.

            if (hello != null) {
                // Log.i(g.TAG, "sendIp=" + broadcastIp);
                try {
                    String ipAddress = Formatter.formatIpAddress(ip);
                    DatagramSocket s = new DatagramSocket();
                    InetAddress local = InetAddress.getByName(ipAddress);//my broadcast ip
                    int msg_length = hello.length();
                    byte[] message = hello.getBytes();
                    DatagramPacket p = new DatagramPacket(message, msg_length, local, g.getPort());
                    Log.v(g.TAG, "Sent hello to " + ipAddress);
                    s.send(p);
                } catch (Exception e) {
                    Log.e(g.TAG, "error  " + e.toString());
                }
            }

            try {
                Thread.sleep(10000); // 10 seconds
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            g.updateScreen();
        }

        // return ("Sender service complete " + startId);
    }

    @Override
    protected void onPostExecute(String result) {
        Log.i(g.TAG, result);
    }

    @Override
    protected void onPreExecute() {
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);

        Log.i(g.TAG, "Service running " + values[0]);
    }
}

public class BroadcastSender extends IntentService {

    GlobalData g = GlobalData.getInstance();
    public static final String CHANNEL_ID = "Sender";

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public BroadcastSender(String name) {
        super(name);
        Log.i(g.TAG, "BroadcastSender constructor");
    }

    public BroadcastSender() {
        super("default");
        Log.i(g.TAG, "BroadcastSender constructor");
    }
    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        Log.i(g.TAG, "onHandleIntent");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(g.TAG, "BroadcastSender onCreate");
    }

    @Override
    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {
        createNotificationChannel();

        Notification notif = g.getNotification();

        if (notif == null) {
            notif = new NotificationCompat.Builder(this, CHANNEL_ID)
                    .setContentTitle("Datecast")
                    .setContentText("Datecast is running")
                    .build();

            g.setNotification(notif);
        }

        startForeground(100, notif);

        AsyncTask task = new SenderTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, startId);
        return Service.START_STICKY;
    }

    private void createNotificationChannel() {
            NotificationChannel serviceChannel = new NotificationChannel(
                    CHANNEL_ID,
                    "Foreground Service Channel",
                    NotificationManager.IMPORTANCE_DEFAULT
            );

            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);
    }
}
