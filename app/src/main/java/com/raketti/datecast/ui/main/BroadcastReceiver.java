package com.raketti.datecast.ui.main;

import android.annotation.SuppressLint;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.graphics.Color;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.raketti.datecast.GlobalData;
import com.raketti.datecast.R;
import com.raketti.datecast.UserData;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;

import static android.content.Context.WIFI_SERVICE;
import static com.raketti.datecast.MainActivity.mainActivity;

class ReceiverTask extends AsyncTask<Integer, Integer, String> {

    GlobalData g = GlobalData.getInstance();

    @Override
    protected String doInBackground(Integer... integers) {

        Log.i(g.TAG, "Receiver running");
        try (DatagramSocket s = new DatagramSocket(g.getPort())) {
            s.setBroadcast(true);

            while (true) {
                try {

                    byte[] buffer = new byte[1024];
                    DatagramPacket packet = new DatagramPacket(buffer, buffer.length);

                    while (true) {
                        s.receive(packet);
                        InetAddress ip = packet.getAddress();
                        String msgbuf = new String(buffer, 0, packet.getLength());
                        Log.d("datecast", "message received from " + ip.toString() + ":" + msgbuf);

                        JSONObject msg;
                        msg = new JSONObject(msgbuf);
                        handleMessage(msg, ip);
                    }

                } catch (Exception e) {
                    Log.e("datecast", "receive error  " + e.toString());
                }
            }
        } catch (SocketException e) {
            e.printStackTrace();
        }

        return "receiverTask";
    }

    private void handleMessage (@org.jetbrains.annotations.NotNull JSONObject msg, InetAddress ip) throws JSONException {

        if (!msg.getString("magic").equals(g.TAG)) // Bad message
            return;

        int uid = msg.getInt("uid");

        if (uid == g.getUid()) // From me
            return;

        Log.v(g.TAG, "From uid " + uid);

        // Check for match
        int age = msg.getInt("age");
        Log.v(g.TAG, "age " + age);

        if (age < g.getMinAge() || age > g.getMaxAge()) {
            Log.v(g.TAG, "Wrong age (" + g.getMinAge() + "," + g.getMaxAge() + ")");
            return;
        }

        int minage = msg.getInt("minage");
        Log.v(g.TAG, "minage " + minage);
        int maxage = msg.getInt("maxage");
        Log.v(g.TAG, "maxage " + maxage);

        if (g.getAge() < minage || g.getAge() > maxage)
            return;

        int sex = msg.getInt("mysex");
        Log.v(g.TAG, "sex " + sex);

        if ((sex & g.getSexMask()) == 0) {
            return;
        }

        int interest = msg.getInt("interest");
        Log.v(g.TAG, "interest " + interest);

        if ((g.getSex() & interest) == 0) {
            return;
        }

        // We have a match

        String nick = msg.getString("nick");
        Log.d(g.TAG, "Match " + nick);
        UserData u = g.getPeer(uid);

        if (u == null) {
            u = new UserData();
            u.uid = uid;
            u.nick = nick;
            u.age = age;
            u.sex = sex;
            u.ip = ip;
            u.time = 0;
            u.time = Calendar.getInstance().getTimeInMillis();
            g.putPeer(uid, u);
            Log.i(g.TAG, "New peer " + nick);

            mainActivity.sendNotification(uid, "New contact", nick);

            g.updateScreen();
        } else {
            u.ip = ip;
            u.time = Calendar.getInstance().getTimeInMillis();
        }

        int event = msg.getInt("event");
        if (event == 1) {
            String chatMsg = msg.getString("msg");
            Log.i(g.TAG, "Received chat: " + chatMsg);

            String chatText = u.chat;

            if (chatText != null)
                chatText += "\n";
            chatText += "-> " + chatMsg;
            u.chat = chatText;
            g.updateScreen();
        }

    }
}

public class BroadcastReceiver extends IntentService {

    GlobalData g = GlobalData.getInstance();
    public static final String CHANNEL_ID = "Receiver";

    public BroadcastReceiver(String name) {
        super(name);
    }

    public BroadcastReceiver() {
        super("default");
        Log.i(g.TAG, "BroadcastReceiver constructor");
    }

    @Override
    public void onCreate() {
        super.onCreate();


    }


    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
    }

    @Override
    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {
        createNotificationChannel();
        Notification notif = g.getNotification();
        if (notif == null) {
            notif = new NotificationCompat.Builder(this, CHANNEL_ID)
                    .setContentTitle("Datecast")
                    .setContentText("Datecast is running")
                    .build();
            g.setNotification(notif);
        }

        startForeground(100, notif);

        AsyncTask task = new ReceiverTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, startId);
        return Service.START_STICKY;
    }

    private void createNotificationChannel() {
            NotificationChannel serviceChannel = new NotificationChannel(
                    CHANNEL_ID,
                    "Foreground Service Channel",
                    NotificationManager.IMPORTANCE_DEFAULT
            );

            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);

    }
}
