package com.raketti.datecast.ui.main;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.raketti.datecast.GlobalData;
import com.raketti.datecast.R;

import org.json.JSONException;
import org.json.JSONObject;

import static android.content.Context.WIFI_SERVICE;
import static com.raketti.datecast.MainActivity.mainActivity;
import static com.raketti.datecast.R.id.errorMessageLine;
import static com.raketti.datecast.R.id.sexGroup;

public class MainFragment extends Fragment {

    private GlobalData g;
    private JSONObject json;

    public MainFragment() {
        g = GlobalData.getInstance();
        json = g.getJson();
    }

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.main_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        try {
            populateFields(json);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Button button = getView().findViewById(R.id.startButton);

        button.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {
                TextView tv = getView().findViewById(errorMessageLine);

                try {
                    try {
                        json.put("magic", g.TAG);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    EditText editText;
                    editText = getView().findViewById(R.id.nickText);
                    final CharSequence nick = editText.getText();

                    WifiManager wifiManager = (WifiManager)mainActivity.getSystemService(WIFI_SERVICE);
                    WifiInfo wifiInfo = wifiManager.getConnectionInfo();
                    int ip;
                    ip = wifiInfo.getIpAddress();

                    if (ip == 0) {
                        tv.setText(R.string.no_wifi);
                        return;
                    }

                    json.put("event", 0);

                    if (nick == "") {
                        tv.setText(R.string.no_nick);
                        tv.setTextColor(Color.RED);
                        return;
                    }

                    Log.i(g.getTAG(), "Nick is '" + nick + "'");
                    try {
                        json.put("nick", nick);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    editText = getView().findViewById(R.id.ageText);
                    int age = Integer.parseInt(editText.getText().toString());
                    if (age < 25 || age > 120) {
                        tv.setText(R.string.bad_age_error);
                        return;
                    }

                    g.setAge(age);

                    try {
                        json.put("age", age);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    RadioButton rb = getView().findViewById((R.id.maleButton));
                    final boolean isMale = rb.isChecked();
                    Log.i(g.getTAG(), "isMale=" + isMale);

                    rb = getView().findViewById((R.id.femaleButton));
                    final boolean isFemale = rb.isChecked();
                    Log.i(g.getTAG(), "isFemale=" + isFemale);

                    if (!isFemale && !isMale) {
                        tv.setText(R.string.no_sex);
                        return;
                    }

                    int sex = (isMale) ? g.SEX_MALE : g.SEX_FEMALE;

                    g.setSex(sex);
                    try {
                        json.put("mysex", sex);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    CheckBox cb = getView().findViewById(R.id.maleCheck);
                    final boolean wantMale = cb.isChecked();
                    Log.i(g.getTAG(), "wantMale=" + wantMale);

                    cb = getView().findViewById(R.id.femaleCheck);
                    final boolean wantFemale = cb.isChecked();
                    Log.i(g.getTAG(), "wantFemale=" + wantFemale);

                    if (!wantMale && !wantFemale) {
                        tv.setText("Want male or female companion?");
                        return;
                    }

                    int want = 0;
                    if (wantMale){
                        want |= g.SEX_MALE;
                    }

                    if (wantFemale){
                        want |= g.SEX_FEMALE;
                    }

                    try {
                        json.put("interest", want);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    g.setSexMask(want);

                    editText = getView().findViewById(R.id.minageText);
                    int minAge = Integer.parseInt(editText.getText().toString());
                    if (minAge < 25 || minAge > 120) {
                        tv.setText(R.string.bad_age_error);
                        return;
                    }

                    try {
                        json.put("minage", minAge);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    g.setMinAge(minAge);

                    editText = getView().findViewById(R.id.maxageText);
                    int maxAge = Integer.parseInt(editText.getText().toString());
                    if (maxAge < 25 || maxAge > 120) {
                        tv.setText(R.string.bad_age_error);
                        return;
                    }

                    if (minAge > maxAge) {
                        tv.setText(R.string.invalid_age_range);
                        return;
                    }

                    try {
                        json.put("maxage", maxAge);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    g.setMaxAge(maxAge);

                    tv.setText("");

                    g.setHello(g.getJson().toString());
                    storeSettings(json);

                    // Finally invoke the second screen
                    Navigation.findNavController(v).navigate(R.id.mainToSecond);
                    g.updateChat();

                    // TODO: Start services
                } catch (NumberFormatException | JSONException e) {
                    e.printStackTrace();
                    tv.setText(R.string.unknown_error);
                }
            }
        });
    }

    public void storeSettings(JSONObject json) throws JSONException {
        SharedPreferences sharedPreferences =
                PreferenceManager.getDefaultSharedPreferences(this.getActivity());

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("nick", json.getString("nick"));
        editor.putInt("age", json.getInt("age"));
        editor.putInt("minage", json.getInt("minage"));
        editor.putInt("maxage", json.getInt("maxage"));
        editor.putInt("mysex", json.getInt("mysex"));
        editor.putInt("interest", json.getInt("interest"));
        editor.commit();
    }

    public void populateFields(JSONObject json) throws JSONException {
        String nick = json.getString("nick");
        EditText editText;
        editText = getView().findViewById(R.id.nickText);
        editText.setText(nick);

        int age = json.getInt("age");
        editText = getView().findViewById(R.id.ageText);
        editText.setText(Integer.toString(age));

        int minage = json.getInt("minage");
        editText = getView().findViewById(R.id.minageText);
        editText.setText(Integer.toString(minage));

        int maxage = json.getInt("maxage");
        editText = getView().findViewById(R.id.maxageText);
        editText.setText(Integer.toString(maxage));

        RadioGroup rg = getView().findViewById(sexGroup);
        int mysex = json.getInt("mysex");

        if ((mysex & g.SEX_MALE) != 0) {
            ((RadioButton)rg.getChildAt(0)).setChecked(true);
        } else {
            ((RadioButton)rg.getChildAt(1)).setChecked(true);
        }

        CheckBox cb = getView().findViewById(R.id.maleCheck);
        int interest = json.getInt("interest");

        if ((interest & g.SEX_MALE) != 0) {
            cb.setChecked(true);
        } else {
            cb.setChecked(false);
        }

        cb = getView().findViewById(R.id.femaleCheck);
        if ((interest & g.SEX_FEMALE) != 0) {
            cb.setChecked(true);
        } else {
            cb.setChecked(false);
        }
    }
}