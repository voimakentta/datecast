package com.raketti.datecast;

import java.net.InetAddress;
import java.util.Date;

public class UserData {
    public int uid;
    public String nick;
    public int age;
    public int sex;

    public InetAddress ip;
    public long time;

    public String chat="";
}
